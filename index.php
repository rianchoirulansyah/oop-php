<?php

    echo "<h3>Biodata Hewan</h3>";

    require_once('animal.php');
    
    $sheep = new Animal("shaun");
    echo "Name : ".$sheep->name. "<br>";
    echo "legs : ".$sheep->legs. "<br>";
    echo "cold blooded : ".$sheep->cold_blooded. "<br>";

    require_once('frog.php');

    echo "<br>";
    $kodok = new Frog("buduk");
    echo "Name : ".$kodok->name. "<br>";
    echo "legs : ".$kodok->legs. "<br>";
    echo "cold blooded : ".$kodok->cold_blooded. "<br>";
    echo $kodok->Jump();
    echo "<br>";

    require_once('ape.php');

    $sungokong = new Ape("kera sakti");
    echo "Name : ".$sungokong->name. "<br>";
    echo "legs : ".$sungokong->legs. "<br>";
    echo "cold blooded : ".$sungokong->cold_blooded. "<br>";
    echo $sungokong->Yell();
    

?>